! Developed by Dhiraj Kumar Hazra (dhirajhazra@gmail.com)
! Here we initialize, allocate and integrate. This is the main engine of the code
module engine
    USE RKSUITE_90_PREC
    use parameters
    implicit none
    real(wp),allocatable,dimension(:,:)::used_mobility_matrix
    contains 
    subroutine initialize(num_states,num_categories)
    implicit none
    integer,intent(in)::num_states,num_categories
        ! Initiate the arrays
    allocate(write_filename(people_categories,states))
    allocate(write_infected_filename(states))    
    allocate(selected_beta_array(states))
    allocate(initial_conditions(num_base_eq,people_categories,states),S_INI(people_categories,states)&
    &,E_INI(people_categories,states),R_INI(people_categories,states)&
    &,beta_matrix(people_categories,people_categories,states),mobility_matrix(people_categories,states,states))
    allocate(used_mobility_matrix(states,states))
    allocate(eff_asymp(states),eff_symp(states),eff_mild(states),eff_svr(states))
    allocate(tau_asymp(states),tau_symp(states),tau_mild(states),tau_svr(states))
    allocate(contact(people_categories,people_categories,states))
    allocate(Lockdown_contact(people_categories,people_categories,states))
    allocate(Unlock_contact(people_categories,people_categories,states))
    allocate(ISY_INI(people_categories,states))
    allocate(IASY_INI(people_categories,states))
    allocate(IM_INI(people_categories,states))
    allocate(ISV_INI(people_categories,states))
    allocate(IH_INI(people_categories,states))    
    allocate(D_INI(people_categories,states))
    allocate(Initial_E(states))
    allocate(Initial_I_ASY(states))
    allocate(Initial_I_SY(states))
    allocate(Initial_I_MILD(states),Initial_I_SVR(states),Initial_I_HPTL(states))
    allocate(C_E2IASY(people_categories,states))
    allocate(C_E2ISY(people_categories,states))
    allocate(C_IASY2R(people_categories,states))
    allocate(C_ISY2MLD(people_categories,states))
    allocate(C_ISY2SVR(people_categories,states))
    allocate(C_MLD2R(people_categories,states))
    allocate(C_SVR2HPTL(people_categories,states))
    allocate(C_HPTL2D(people_categories,states))
    allocate(C_HPTL2R(people_categories,states))
    allocate(C_SVR2R(people_categories,states))
    allocate(C_SVR2D(people_categories,states))    
    allocate(fraction_AS(people_categories),fraction_MILD(people_categories))
    allocate(fraction_dead(people_categories))
    allocate(fraction_NH_dead(people_categories))
    allocate(fraction_HPTL(people_categories,states))
    end subroutine initialize
    
    subroutine Calc_initial_conditions(these_initials,these_populations)
      use RKSUITE_90_PREC
      use parameters
      implicit none
      Real(WP),dimension(:,:,:),intent(out)::these_initials
      Real(WP)::scaling
      integer,dimension(:),intent(in)::these_populations
      REAL(WP),dimension(size(these_initials,2),size(these_initials,3))::population_in_this_patch
      Integer::I,J,K,total_branches
      
      
      !Initial_E=500d0 ! 5 people uniformly for all states and all categories
      !Initial_I_ASY=100d0 ! No people infected
      !Initial_I_SY=150d0 ! No people infected
      
!       print*,people_categories
      
      do k=1,states    
         do j=1,people_categories
            ! Equally dividing in different categories
            ! Will be replaced with actual numbers
            if(people_categories>1) then 
            if(j<people_categories) population_in_this_patch(j,k)&
                &=dble(these_populations(k))*population_fraction(j,k)
            if(j==people_categories) population_in_this_patch(j,k)&
                &=dble(these_populations(k))-sum(population_in_this_patch(1:j-1,k))
            !print*,'population_in_this_patch',dble(population_in_this_patch(j,k))
            !pause
!             print*,population_in_this_patch(j,k)
            else 
            population_in_this_patch(j,k)=dble(these_populations(k))
            end if
!             scaling=1d0
!             if(allocated(scale_IC)) scaling=scale_IC(j)
            E_ini(j,k)= Initial_E(k)*population_fraction(j,k)
            IASY_INI(j,k)= Initial_I_ASY(k)*population_fraction(j,k)
            ISY_INI(j,k)= Initial_I_SY(k)*population_fraction(j,k)            
            IM_INI(j,k)=Initial_I_MILD(k)*population_fraction(j,k)      
            ISV_INI(j,k)=Initial_I_SVR(k)*population_fraction(j,k)
            IH_INI(j,k)=Initial_I_HPTL(k)*population_fraction(j,k)            
            S_ini(j,k)=population_in_this_patch(j,k)-E_INI(j,k)&
            &-IASY_INI(j,k)-ISY_INI(j,k)-IM_INI(j,k)-ISV_INI(j,k)-IH_INI(j,k) ! Initial susceptible
            D_ini(j,k)=0d0 !No DEAD people to start with             
            R_ini(j,k)=0d0 !No Recovered people to start with 
!             print*,'Initial CONDITIONS:'
!             print*,E_ini(j,k),IASY_INI(j,k),ISY_INI(j,k),IM_INI(j,k),ISV_INI(j,k),IH_INI(j,k),S_INI(j,k)
!             pause
         end do
      end do    
            
!       if(.not.(allocated(scale_IC))) pause 'Branching not required ? check'      
            
     ! These are just values to play with 
     ! Will write another routine or make it a part of initialize
     beta_matrix=0d0
     ! beta will be populated in each day during the intergration. Here we just initialize
     
     if(Mobility) then 
     ! Populating from Migration Matrix
     ! Now assuming migration is Uniform across the categories
     ! and only depends on states
        do j=1,states
            do k=1,states
               if(Simulation_system=='Country') then
                if(granularity=='States') used_mobility_matrix(j,k)&
                &=Entire_Migration_matrix(selected_states_indices(j),selected_states_indices(k))

                if(granularity=='Districts') used_mobility_matrix(j,k)&
                &=Entire_Migration_matrix(selected_district_indices(j),selected_district_indices(k))
                                
              ENDIF  
            end do
        end do
        
        If(Simulation_system=='Generic') used_mobility_matrix=Entire_Migration_matrix
!       print*,'Mobility',used_mobility_matrix
!       pause
     else 
        used_mobility_matrix=0d0
     end if
     
!      print*,'mobility_matrix currently not used: to use mixing, read from a data file'
!      print*,'Compiler Bug hits when forall in Mobility used'
     !1d-2
     
    
    ! Mortality and Birth neglected
    do k=1,states    
        do j=1,people_categories
            C_E2IASY(j,k)= E_ASY*fraction_AS(j) ! Coefficient for Exposed to Asymptomatic
            C_E2ISY(j,k)= E_SY*(1d0-fraction_AS(j))! Coefficient for Exposed to symptomatic
            C_IASY2R(j,k)= ASY_R! Coefficient for Asymptomatic to Recovered
            C_ISY2MLD(j,k)= SY_M*fraction_MILD(j)! Coefficient for symptomatic to mild
            C_MLD2R(j,k)=  M_R ! Coefficient for mild to Recovered     
            C_ISY2SVR(j,k)= SY_SV*(1d0-fraction_MILD(j))! Coefficient for symptomatic to severe
            C_SVR2HPTL(j,k)= SV_H*fraction_HPTL(j,k)! Coefficient for severe to hospitalized
            C_SVR2R(j,k)= SV_R*(1d0-fraction_HPTL(j,k))*(1d0-fraction_NH_dead(j))! Coefficient for severe to Recovered
            C_SVR2D(j,k)= SV_D*(1d0-fraction_HPTL(j,k))*fraction_NH_dead(j)! Coefficient for severe to hospitalized            
            C_HPTL2D(j,k)= H_D*fraction_dead(j)! Coefficient for hospitalized to dead
            C_HPTL2R(j,k)= H_R*(1d0-fraction_dead(j))! Coefficient for hospitalized to Recovered
        
            these_initials(susceptible,j,k)=S_ini(j,k)             
            these_initials(exposed,j,k)=E_ini(j,k)             
            these_initials(asymptomatic,j,k)=IASY_INI(j,k)             
            these_initials(symptomatic,j,k)=ISY_INI(j,k)                         
            these_initials(mild,j,k)=IM_INI(j,k)                         
            these_initials(severe,j,k)=ISV_INI(j,k)                         
            these_initials(hospitalized,j,k)=IH_INI(j,k)                         
            these_initials(dead,j,k)=D_INI(j,k)                         
            these_initials(Recovered,j,k)=R_ini(j,k)                                     
        end do
    end do    

!   print*,these_initials
!   pause
!      print*,these_initials(susceptible,:,:),S_ini(:,:) 
      ! MAYBE WE SHOULD READ THE INITIAL CONDITIONS FROM A FILE
      
    end subroutine Calc_initial_conditions
    
   subroutine integrate(these_initials,these_finals,these_days)
    USE RKSUITE_90_PREC
    USE RKSUITE_90
    Use system_of_equations
    implicit none
    REAL(WP),DIMENSION(:,:,:),Intent(IN)::these_initials    
    REAL(WP),DIMENSION(:,:,:,:),Intent(OUT)::these_finals
    REAL(WP),DIMENSION(:),Intent(IN)::these_days    
    REAL(WP),DIMENSION(:,:,:),allocatable::DUMMY_VECTOR,Final_this_day
    LOGICAL,DIMENSION(:,:,:),allocatable::THIS_MASK    
    Real(WP),dimension(:),allocatable::Y_GOT,YDERIV_GOT,THRESHOLDS,initials
    Real(WP),dimension(:,:),allocatable::SEIR_OUT
    REAL(WP)::date_int_success,TOLERANCE,this_day_end,finish,start
    Integer::I,FLAG,j,state_loop,category_loop,Today,category_loop1,category_loop2
    REAL(WP),DIMENSION(states)::This_infected,This_Hospitalized        
    TYPE(RK_COMM_REAL_1D) :: COMMB
    
    allocate(initials(size(these_initials)))

!             call cpu_time(start)        

    initials=pack(these_initials,.True.)        
!         call cpu_time(finish)
!         print*,'packing 1 took time',finish-start
    
    ALLOCATE(Y_GOT(size(initials)),YDERIV_GOT(size(initials)),THRESHOLDS(size(initials)))
    ALLOCATE(SEIR_OUT(size(these_days),size(initials)))
    TOLERANCE=1.0D-3
    THRESHOLDS=1.0D-4

    SEIR_OUT(1,:)=initials

    allocate(DUMMY_VECTOR(num_base_eq,people_categories,states),THIS_MASK(num_base_eq,people_categories,states))    
    allocate(Final_this_day(num_base_eq,people_categories,states))
    ! Storing the values after unpacking 
    THIS_MASK=.True.
    
!     print*,initials
!     print*,these_initials
!     pause
    ! If lockdown is not there, these should be set to 1. Used in syseq.f90
    
    
    ADAPTIVE_SEIR:DO I=1,steps_days-1

    ! Decide for lockdown beta matrix
        forall(state_loop=1:states) beta_matrix(:,:,state_loop)=selected_beta_array(state_loop)  
        forall(category_loop=1:people_categories) mobility_matrix(category_loop,:,:)=used_mobility_matrix(:,:)
        
        tau_asymp(:)=decay_time_asymp_Unlock
        tau_symp(:)=decay_time_symp_Unlock
        tau_mild(:)=decay_time_mild_Unlock
        tau_svr(:)=decay_time_svr_Unlock

        
        if(Implement_lockdown) then     
            do state_loop=1,states
            
            if(any(lockdown(state_loop,:,I))) then
                tau_asymp(state_loop)=decay_time_asymp
                tau_symp(state_loop)=decay_time_symp
                tau_mild(state_loop)=decay_time_mild
                tau_svr(state_loop)=decay_time_svr
                if(.not.(Is_Uniform_contact)) contact(:,:,state_loop)=Lockdown_contact(:,:,state_loop)
             else 
                if(.not.(Is_Uniform_contact)) contact(:,:,state_loop)=Unlock_contact(:,:,state_loop)
             end if    
                do category_loop1=1,people_categories
                    if(lockdown(state_loop,category_loop1,I)) then             
                        beta_matrix(category_loop1,:,state_loop)=selected_beta_lockdown_array(state_loop)
                        mobility_matrix(category_loop1,state_loop,:)=eff_mob_block*used_mobility_matrix(state_loop,:)
!                         print*,beta_matrix(category_loop1,:,state_loop)
!                         pause
                    else 
                        do category_loop2=1,people_categories
                            if(lockdown(state_loop,category_loop2,I)) &
                            &beta_matrix(category_loop1,category_loop2,state_loop)=selected_beta_lockdown_array(state_loop)          
!                         print*,beta_matrix(category_loop1,category_loop2,state_loop)
!                         pause
                            
                        end do
                    end if
                end do
            end do
        end if
    
     
      
!          print*,'FROM Engine'
!          do category_loop=1,people_categories
!             print*,I,tau_asymp,tau_symp,tau_mild,tau_svr          
!           print*,I,mobility_matrix(category_loop,:,:)
!           print*,contact(category_loop,:,1)
!          end do
!         print*,'DID YOU CHECK Mobility BLOCK FOR STAGGERED LOCKDOWN ?'
!          pause
            
    
        CALL SETUP(COMMB,these_days(I),initials,these_days(I+1),TOLERANCE,THRESHOLDS,method="H",MESSAGE=.TRUE.)
    
             
        this_day_end=these_days(I+1)
        
        CALL range_integrate(COMMB,system_eq,this_day_end,date_int_success,Y_GOT,YDERIV_GOT,FLAG=FLAG)
        IF(FLAG/=1) THEN
            PRINT*,"INTEGRATION NOT SUCCESSFUL"
            PRINT*,'COULD EVALUATE TILL day=',date_int_success,"INSTEAD N=",this_day_end
            IF(ABS(date_int_success-this_day_end)>1.0d-8) STOP "EXITING"
        ENDIF
        !==================== STORE INTERMEDIATE VALUES TILL NEND ==================
        SEIR_OUT(I+1,:)=Y_GOT(:)    
    
    
        Final_this_day=0d0
        Final_this_day(:,:,:)=unpack(SEIR_OUT(I+1,:),THIS_MASK,dummy_vector)
        
        Today=Int(This_day_end)
        
        do state_loop=1,states
        
            if(Implement_lockdown.and.Lightswitch) then 
            This_infected(state_loop)=0d0
            if(Hospital_Controlled) This_Hospitalized(state_loop)=0d0
            end if
            do category_loop=1,people_categories
                if ((Final_this_day(asymptomatic,category_loop,state_loop)<1d0)&
                    &.and.(Final_this_day(symptomatic,category_loop,state_loop)<1d0)&
                    &.and.(Final_this_day(mild,category_loop,state_loop)<1d0)&
                    &.and.(Final_this_day(severe,category_loop,state_loop)<1d0)) then 
                    Final_this_day(asymptomatic:severe,category_loop,state_loop)=0d0
                end if
                if(.not.(any(Final_this_day(asymptomatic:hospitalized,category_loop,state_loop)>1d-5))) &
                &Final_this_day(asymptomatic:hospitalized,category_loop,state_loop)=0d0
                
                 if(Implement_lockdown.and.Lightswitch) then 
                    This_infected(state_loop)=This_infected(state_loop)&
                        &+sum(Final_this_day(asymptomatic:severe,category_loop,state_loop)) 
                    if(Hospital_Controlled) This_Hospitalized(state_loop)&
                 &=This_Hospitalized(state_loop)+Final_this_day(hospitalized,category_loop,state_loop)
                 end if
            end do 
!         print*,'infected people',This_infected(state_loop),'Day',Today      
        if(Implement_lockdown) then 
            if(Lightswitch.and.(.not.(Hospital_Controlled)).and.(.not.(all(Lockdown(state_loop,:,Today))))&
                    &.and.This_infected(state_loop)>Lightswitch_Threshold)&
            &Lockdown(state_loop,:,Today:min(Today+Lightswitch_duration-1,int(DAY_FINAL)))=.True.

            ! When Hospital_Controlled lockdown is implemented, lockdown when the number crosses THRESHOLD
            if(Lightswitch.and.Hospital_Controlled.and.This_Hospitalized(state_loop)>Hospital_Threshold) &
            &Lockdown(state_loop,:,Today:int(DAY_FINAL))=.True.
 
            ! Lift the hospital Hospital_Controlled lockdown
       
            if(Lightswitch.and.Hospital_Controlled.and.This_Hospitalized(state_loop)<Lightswitch_off_Tol) &
            &Lockdown(state_loop,:,Today:int(DAY_FINAL))=.False.
        end if
        
!           print*,'hospitalized people',This_Hospitalized(state_loop),'Day',Today
!         print*,Lockdown
!         pause 'Lightswitch activated'        
!         end if
            
        end do
        
!         call cpu_time(start)        
        initials=pack(Final_this_day,.True.)        
!         call cpu_time(finish)
!         print*,'packing 2 took time',finish-start
        
        CALL COLLECT_GARBAGE(COMMB)
        
     END DO ADAPTIVE_SEIR
        
    
    
!         call cpu_time(start)        

    do i=1,size(these_days)
        these_finals(i,:,:,:)=unpack(SEIR_OUT(i,:),THIS_MASK,dummy_vector)
    end do
!         call cpu_time(finish)
!         print*,'unpacking took time',finish-start
    
    end subroutine integrate
end module engine
