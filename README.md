# ELiXSIR -- Extended, zone Linked IX-compartmental SIR model: a code to simulate COVID19 infection

## System of equations

This code follows the **INDSCI-SIM** model. The code has 9 compartments namely:

```
1: susceptible (S)

2: exposed (E)

3: asymptomatic (Ia)

4: pre-symptomatic (Ip)

5: mild (Im)

6: severe (Is)

7: hospitalized (H)

8: dead (D)

9: recovered (R)
```

The code is able to take input of N patches and M types of population. It solves 9*N*M equations with adaptive stepsize control RKSUITE. This code allows mixing between M types and explicit migration between N patches.


Dead and recovered are removed from system. 

All severe cases are hospitalized.

All mild gets recovered. 

All asymptomatic gets recovered. 

Severe can go to three compartments, hospitalized, recovered and dead. In ideal cases all severely ill people should go to the hospital. However, due to large fluctuations in the availablity of health care across different regions in the country, we have included these links. 

The code neglects birth and death rates (due to natural causes)

The full set of equations are provided below:

![ELiXSIR equations](Equations-and-Diagrams/Equations.png)
 
 
The structure of the equations is provided below:

```mermaid
graph TD;
  Susceptible-->Exposed;
  Exposed-->Asyptomatic;
  Exposed-->Pre-symptomatic;
  Asyptomatic-->Recovered;
  Pre-symptomatic-->Mild;
  Pre-symptomatic-->Severe;  
  Mild-->Recovered;
  Severe-->Recovered;  
  Severe-->Hospitalized;  
  Severe-->Dead;  
  Hospitalized-->Recovered;
  Hospitalized-->Dead;
```




## Interventions
ELIXSIR allows several non-pharmaceutical interventions (NPIs). They can be classified into 2 broad categories. Lockdown and tensting followed by quarantining.

### Lockdown

**Multiple Lockdown phases** are allowed. Three types of Lockdowns are implemented.


1. **_Phased lockdown_**: lockdown in different phases. You should provide the starting date and duration of each lockdowns

2. **_Lightswitch lockdown_**: lockdown whenever infected population goes beyond a threshold

   1. Can implement through number of infected threshold and duration 
   
   2. Can also be implemented through hospital controlled switch 

3. **_Cyclic lockdown_**: Lockdown will happen with some brief intervals. This can be of following types
    
    1. _Synchronous lockdown_: all age groups synchronously follow the lockdown
    
    2. _Staggered lockdown_: lockdowns are async in different categories
    
    3. _Selected Periodic Lockdown_: combination of sync and async lockdowns depending on ages

Cyclic lockdown can start with a phased lockdown.

Flowchart for the three types of periodic lockdowns:

![Synchronous Lockdown flowchart](Equations-and-Diagrams/Sync-flowchart.png)![Staggered Lockdown flowchart](Equations-and-Diagrams/Async-flowchart.png)
![_Selected Periodic Lockdown flowchart](Equations-and-Diagrams/Selected-periodic-flowchart.png)
    

### Testing + Quarantining

We assume that testing will improve over time and infected individuals will be quarantined which will in turn will reduce the intensity of the infected compartments to infect the susceptible. We use a exponential decrease in infectivity with a parameter 𝜏. 𝜏 for 4 infected compartments can be different. In Default option we use that 𝜏 for asymptomatic cases are 2 times that of the other cases. During lockdown and unlock modes, 𝜏 can take different values. In this model we demand that after 𝜏 days from the start of the simulation, we will be able to isolate _(1-1/e)~0.63_ fraction of the population in the _Ip, Im_ and _Is_ compartments. Therefore, higher 𝜏 value implies slow testing and quarantining. 

## Migration

Migration between states and districts is implemented. There are two options to allow migration between states and one option for districts. 

1. **_Migration-Census2011_**: This option is only available for migration between states. A file is required with inbound state (column 1), outbound state (column 2) and number (in 100000 units). The file provided with this code lists migration between two states according to Census2011 data. Note that this lists long term migration. Since daily migration should also follow similar pattern, a scaling is required which should be provided in the ini file.

2. **_Gravity_**: This calculates the migration between states and districts by assuming that daily migration between two states/districts are proportional to the product of their populations and inversely proportional to the square of the distance between the states/districts. The proportionality constant G should be provided.

One such example of migration matrix (calculated for Tamil Nadu state using gravity algorithm) is as given below, where the color represents the daily traffic between different districts within Tamil Nadu:

![Migration Matrix](Equations-and-Diagrams/Migration.png)




## Age structuring

Age structuring is implemented rigorously. Arbitrary number of categories can be provided. Categories can be divided by age or any other classes. Population fraction must be provided for all categories. If all regions have similar fractions, _Uniform_Population_Fraction_ should be used and the ini file must contain the fractions _Population_fraction_1_. For non-uniform fractions, input file has to be provided. 

Only asymptomatic fraction is treated as age independent (within the scope of INI file). Minor modification is required to read asymptomatic fraction read from INI file in age dependent manner.

## Contact matrices

ELIXSIR can work with uniform contacts across ages and with age and place dependent contact matrices. For non-uniform matrices it uses the matrices provided in _Prem, Cook and Jit_, https://doi.org/10.1371/journal.pcbi.1005697 and rescale them according to the user defined age groups. However, if _Tailor_Made_Contact_ are flagged in ini file, then the code will read the matrices using the prepared contact matrices for all regions. The extensions should be provided. Consult the ini files for details.

Combined contact matrices during lockdown and unlock modes are determined by linear combinations of the contact matrices obtained for different places (_Home, Work, School and others_). Example of contact matrices during lockdown and unlock times are provided below:

![Contact matrix lockdown](Equations-and-Diagrams/Contact-Lockdown.png)![Contact matrix lockdown](Equations-and-Diagrams/Contact-Unlock.png)

## Simulation System

ELiXSIR can solve 3 types of systems at present. You have to mention the system to solve. 4 sets of ini files are provided as examples.

1. **Single**: Here the system will solve only for one region. Migration will not be used, Population fraction has to be mentioned directly in the parameters.ini file. 

_parameters-single.ini_ is provided as example. 

2. **Country**: This is specifically meant to simulate the spread of the epidemic across the country at different granularity. Country is divided into _states_ and states are divided into _districts_. Use can chose whether to use state level or district level simulation for the country. The granularity, which is a user defined key, can therefore be _**States**_ or _**Districts**_

_parameters-country-state.ini_ and _parameters-country-districts.ini_ are provided for 2 different granularities. 

3. **Generic**: When you do not care about the structure and provides any regions. For example, larger than country and lower than district scale simulation can be performed easily using this setup. 

_parameters-generic.ini_ can be used for generic set-up.

## Input Files

The code requires a few input files to read data. They should be in input_data folder. Note that different simulation systems will have different input files. Here 3 folders are provided (within input_data directory) for 3 systems that ELiXSIR can solve. Note that **_ALL FILES MUST HAVE A HEADER_**. 

1. `Population-file.dat`: Contains the states and population. The format should be (For _Generic_ and _Single_ systems)

    Column 1     | Column 2   |     Column 3     |   Column 4       |    Column 5   |  Column 6 
    -------------|------------|------------------|------------------|---------------|----------- 
    State-Name   | Population | Population-Rural | Population-Urban |	Lattitude   | Longitude 

    If `Simulation_System=Country` then the input population file structure should be slightly different.

    Column 1     | Column 2     | Column 3   |Column 3     | Column 4   |     Column 5     |   Column 6       |    Column 7   |  Column 8 
    -------------|--------------|------------|-------------|------------|------------------|------------------|---------------|-----------
    State-Code   |District-Code | State-Name |District-Name| Population | Population-Rural | Population-Urban |   Lattitude   | Longitude 
    

    `State-Code`, `District-Code` should contain the numbers of states and districts. `State-Name` should be same for all the `District-Name` belonging to a state. If the rural and urban population classification is not needed, you can put zeros in these two columns, but you can not leave them blank. I know it is confusing and I plan to improve this part. If `Allow_Mobility=F` then last 2 columns are not required. 
    
    _State names can not have spaces in between._
    
2. `selected_states.dat`
   
   If you do not want to you all states and union territories, just mention the state names in this file in single column. Only works in `Simulation_System=Country`.

3. `Population-Fraction.dat`
   
   If you work with more than one age group, In _parameters.ini_ you need to mention what are the fractions. If for different states you want to use different fractions then you should prepare this file for all the states. Format should be:
   
    Column 1     | Column 2        | .......              | Column N+1 
    -------------|-----------------|----------------------|--------------------
    State-Name   | Fraction 1      | .......              | Fraction N
   
   
   
4. `Initial.dat`

   If all states are used and initial conditions are different then this file should be prepared. The order of appearance of name of regions must be in accordance with `Population-file.dat`. Format should be:

   If you want to use different initial conditions in different states mention them next to state names (Flag it appropriately in parameters.ini). The format should be:
   
    Column 1     | Column 2  | Column 3 | Column 4 | Column 5 | Column 6 | Column 7 
    -------------|-----------|----------|----------|----------|----------|----------
    State-Name   | Initial-E | Initial-a| Initial-p| Initial-m| Initial-s| Initial-H
  
   Note that these number represent the total numbers across all _categories_. The population fraction in each _categories_ scale these numbers accordingly.
   

   
   
5. `Migration.dat`

   If migration is used and  with _Migration-Census2011_, a file must be provided for populating the mobility matrix. Format should be:

    Column 1          | Column 2           | Column 3 
    ------------------|--------------------|-----------------
    Incoming flux to  | Outgoing flux from | Number in 100000

    You must normalize the numbers to one day flux. See, parameters.ini
    
6. `State-beta-variation.dat`

    If the infectivity, 𝛽 or equivalently _R0_ is not assumed to be constant, then mention that in the _parameters.ini_ file along with the name of the file to read from. 

    Column 1          | Column 2           | Column 3        | Column 4
    ------------------|--------------------|-----------------|------------------ 
    State-Name        |  𝛽 in unlock mode  | Lockdown=T/F    | 𝛽 during lockdown
    
    
    ELiXSIR will impose lockdown in the regions assuming `T=True` and `F=False` and will use the 𝛽 values accordingly. For `Simulation_System=Country and Granularity=Districts`, the code will use the 𝛽 for the states provided. 
    

## Structure of the code    
    
```mermaid
graph TD
  A[ELiXSIR] -->|./elixsir parameters.ini| B{Simulation_System}
  B -->|One Region| D(Single)
  B -->|Country-State-District Setup| E(Country)
  B -->|Arbitrary zones| F(Generic)
	E--> G(Granularity) 	
  G-->H(States)
  G-->I(Districts)
  D-->id1[(Input Database)]
  F-->id1[(Input Database)]
  H-->id1[(Input Database)]
  I-->id1[(Input Database)]
 id1-.-P[Population-file.dat]
id1-.-|If fraction not Uniform|Population-Fraction.dat
id1-.- |If non-Uniform initial condition|Initial.dat
id1-.- |If Migration included|M[Migration.dat]
id1-.-|For System=Country|Selected-States.dat
id1-.-|If R0 is different|State-beta-variation.dat
id1-.-|For non-Uniform Contacts|CM[Contact-Matrices]
CM-.-|If provided for fine grained age structures|BCM[Base-Contact-Matrices]
CM-.-|Ready made matrices|Tailor-Made-Contacts
P-.-PLL
M-.-|Survey Input|Migration.dat
M-.-|Gravity Algorithm|PLL[Population-Lattitude-Longitudes]
BCM-.-BM[Base-Matrices]
BCM-.-BPF[Base-Populaion-Fraction]
BM-.-ENV[Environment]
ENV-.-Home
ENV-.-Work
ENV-.-School
ENV-.-Others
```
    
## Prerequisites

The code is written in Fortran, It is advised to use newer Fortran compilers.

```
Intel Fortran Compiler (ifort)/Gfortran (6+)
gnuplot (for plotting/optional)

```

## How to compile and run

to compile and run the code:

```
make clean

make all

make run
```

Default _make run_ will run the **Generic** system. You should use **_./elixsir parameter-{system}.ini_** to run your own system. Outputs will be produced in plot_data directory. If plot_data directory does not exist in the folder, you need to create that

Actually make run uses parameters.ini to run the program. Different parameters can be changes in the parameters.ini file. Parameters are described in the ini file.

If you have _gnuplot_ installed, do the following:

```
make plots
```
 

## Routines

Small description regarding the routines.

```
parameters.f90: contains the parameters that are used globally

utils.f90: utility files

syseq.f90: system of equations

engine.f90: routines to initiate and integrate 

elixsir.f90: Main driver

(inifile.f90, rksuite.f90): Not developed by me. Routine to read input from inifile and integration routine respectively.
```

## Output:

```
1. plot_data/ELIXSIR_OUT_{_state_name_}_people_category_{_category_}.txt
2. plot_data/ELIXSIR_CUMULATIVE_OUT_{_state_name_}.txt

3. stored_data/contact-store/Reduced-Contact-{_place_}{_state_}.txt: 
4. stored_data/Migration_matrix/Migration.txt (only for Generic system)
```

1 contains files for individual categories while 2 contains the daily and cumulative infected and dead cases. In both the cases, columnheaders are provided. 

If non-uniform place dependent (Home, work, school, others) contact matrix is used from a base contact matrix (_Prem, Cook and Jit_, https://doi.org/10.1371/journal.pcbi.1005697), the code creates the contact matrices for each state depending on the base contact matrix age bracket and the age bracket for the simulation. These rescaled contacts for the simulation age group are written in 3. 

4 contains the Migration matrix estimated using the _Gravity Algorithm_. 



Below are the plotscripts that are generated by the code during runtime. 

```
plots.p: gnuplot script generated by the code to plot the time evolution

Run gnuplot > load 'plots.p' to generate time series data.
make plots will execute this

Mobilityplots.p: This script plots the Migration matrix
Run gnuplot > load 'Mobilityplots.p' to generate display Mobility matrix (Generic only)

Contacts.p: This script plots the contact matrices. 
Run gnuplot > load 'Contacts.p' to generate display contact matrix
```

## INDSCI-SIM analyses based on ELiXSIR

Till now there have been several releases of INDSCI-SIM simulations in public.

The results are available in slide format: 

[Version 1.0: INDSCI-SIM -- A state-level epidemiological model for India](https://docs.google.com/presentation/d/e/2PACX-1vQ4MeAK0dHCUEbNewhMjKS7koH99_8v2lhMyyDmP81Cfuxv4U0OuzC_kL5dbDwtl5V_TF11i0JkmcxM/embed?start=false&loop=false&delayms=5000#slide=id.p)

[Version 1.1: INDSCI-SIM -- Application to periodic lockdowns](https://docs.google.com/presentation/d/e/2PACX-1vS-pCw4S6sebX6pZq_3xTOm3RjksR4ONHzZJ7LYDR886wqZMxMgl3UZhlRg3T2IDatDmg622LmR19I1/embed?start=false&loop=false&delayms=60000#slide=id.p)

[Version 1.2: INDSCI-SIM -- Estimating national lockdown impact](https://docs.google.com/presentation/d/e/2PACX-1vQS3dRmiCz25kHs_aeR84y2M2Aya0WuToQGSP8Sndh0R0yaSfyc7ArcuEm8W4Eg02cVAvEPo0UPMCCP/embed?start=false&loop=false&delayms=3000#slide=id.p)

[Version 1.3a: INDSCI-SIM -- Chennai Projections](https://docs.google.com/presentation/d/e/2PACX-1vRyRtpkpeEBoCXQmMm5p5CBDRhwrLLaTu8V-BcgC9dPKnCgf8QaRTE0PI_MFLsQM--u5xXN3nYQ5YHX/embed?start=false#slide=id.p)

[Version 1.4: INDSCI-SIM -- Delhi analysis](https://docs.google.com/presentation/d/e/2PACX-1vREVok2CII0NNRTj8TY6THNEkUtXmGlade-MQrgOdKZjp5YF7lFp-m0Zsolg7M9xGGEtPzwEKrqWMkH/embed?start=false#slide=id.p)


## Acknowledgements

The code is based on the **INDSCI-SIM** model. I would like to thank the following members of the _INDSCI-SIM_ team who have actively contributed in modelling, resource collection and data analysis -- that in turn helped in the development of ELiXSIR. 

_Pinaki Chaudhuri (IMSc, Chennai)_

_Vishwesha Guttal (IISc, Bengaluru)_

_Gautam I. Menon (Ashoka Univ., Sonipat & IMSc, Chennai)_

_Bhalchandra Pujari (CMS, SPPU, Pune)_

_Snehal Shekatkar (CMS, SPPU, Pune)_

_Sitabhra Sinha (IMSc, Chennai)_


This code uses 2 external routines:

1. inifile.f90: From https://cosmologist.info/cosmomc/readme.html
2. rksuite.f90: From http://www.netlib.org/ode/rksuite/

---

* Developed by Dhiraj Kumar Hazra, IMSc, Chennai, India
* Within the efforts of Indian Scientists' Response to CoViD19
* for bugreport and suggestions, please write to dhirajhazra@gmail.com

  
