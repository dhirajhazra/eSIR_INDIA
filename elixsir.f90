! Developed by Dhiraj Kumar Hazra (dhirajhazra@gmail.com)
! This is the main driver.
program elixsir
  USE RKSUITE_90_PREC
  use parameters
  use system_of_equations
  use utils
  use engine
  USE INIFILE     
  implicit none
  Real(WP),dimension(:),allocatable::save_day,state_lat,state_lon,Initial_E_STORE,Initial_I_ASY_STORE
  Real(WP),dimension(:),allocatable::Initial_I_SY_STORE,Initial_I_MILD_STORE
  Real(WP),dimension(:),allocatable::Initial_I_SVR_STORE,Initial_I_HPTL_STORE,LockDown_Cont_Mat_Coeff,Unlock_Cont_Mat_Coeff
  Real(WP),dimension(:),allocatable::Dist_lat,Dist_lon,beta_array,beta_lockdown_array    
  Real(WP),dimension(:,:,:,:),allocatable::SEIR_STORE_ARRAY    
  Real(WP),dimension(:,:),allocatable::SEIR_New_Infected_Daily,SEIR_Cumulative_Infected
  Real(WP),dimension(:,:),allocatable::SEIR_Cumulative_Dead,base_population_fraction        
  real(wp)::migration_in_lakhs,ratio_dead_NH_H,this_lat,this_lon,const_G
  integer::Cycle_duration_Async,Cycle_break_Async,Num_Base_Age_Group,contact_filenum,all_states
  integer::i,j,k,filenum_data,this_state,this_state_num(1),filenum_result,Num_M_data,This_District,counter
  integer::idx(1),jdx(1),pop1,pop2,Migration_duration,Num_corr,dummy,Num_Entry,Num_states,District_counter
  integer::This_District_id,This_state_id,Dist_Num,This_population,This_Population_Rural,This_population_urban
  integer::Lockdown_start_day,Lockdown_duration,Num_Lockdown,this_day,This_Cycle_start_day,filenum_result_infected
  integer,allocatable,dimension(:)::First_district,Last_district,Selected_State_ID_of_DIST,Age_bracket
  integer,dimension(Total_District_Num)::State_ID_of_DIST,Population_district  
  integer,allocatable,dimension(:)::Dummy_selected_district_indices,first_phase
  Integer(WP),dimension(:),allocatable::population
  character(Len=32)::this_category
  character(LEN=Ini_max_string_len)::This_state_name,This_district_name,key_bracket,population_fraction_filename
  character(LEN=Ini_max_string_len),dimension(Total_District_Num)::district_names,District_Belongs_To
  character(LEN=Ini_max_string_len),allocatable,dimension(:)::selected_district_belongs_to,Store_selected_states
  character(LEN=Ini_max_string_len),allocatable,dimension(:)::Base_Matrix_Filename,write_contact_name
  character(LEN=Ini_max_string_len)::datafile,status,foldername,key_frac,write_contact_mat,key_asymp
  character(LEN=Ini_max_string_len)::this_lockdown,state1,state2,category_num,key_name,Mobility_Algorithm,BETA_FILENAME,This_name
  character(LEN=Ini_max_string_len)::InputFile,key_lock_day,key_lock_duration,key_mild,key_dead,command_line,Staggered_key 
  logical::bad,Implement_Rapid_Quarantine_Effect,Uniform_fraction,Uni_Pop_Frac,BETA_STATE_VARIATION
  logical::Branch_Categories,Selected_Periodic,start_with_Lockdown,Write_Derived_Contact
  logical,dimension(Total_District_Num)::is_rural
  logical,dimension(:),allocatable::Staggered_Cyclic
  
  !INIFILE Initialization
  !=================== Input file   =========================
  InputFile = ''

  if (iargc() /= 0)  call getarg(1,InputFile)
  if (InputFile == '') stop 'No parameter input file'

  call Ini_Open(InputFile, 1, bad, .false.)
  if (bad) stop 'Error opening parameter file'
  Ini_fail_on_not_found = .false.


  Simulation_system=Ini_Read_string('System')


  !========  Reading from file ===============================
  datafile=Ini_Read_string('State_filename')
  
  if(Simulation_system=='Country') then 
  Granularity=Ini_Read_string('Granularity')
  if(Granularity/='States'.and.Granularity/='Districts') stop 'Granularity not correct'
  end if  
  ! Read data (state names and population)
  ! Read only states and not UT for now. 
  ! loop over 29 states and 7 UTs 

  Mobility=Ini_Read_Logical('Allow_Mobility',.False.)    
       
  allocate(First_district(total_state_num),Last_district(total_state_num))
  if(Simulation_system=='Country') then 
    allocate(population(total_state_num)) 
    if(Mobility) then 
        allocate(state_lat(total_state_num),state_lon(total_state_num))      
        allocate(Dist_lat(Total_District_Num),Dist_lon(Total_District_Num))
    end if
  end if  
    
  Print*,'Reading data from:',datafile

  
 ! Get the length of the file  
    command_line="wc -l < "//trim(adjustl(datafile))//" > linenum.txt" 
    print*,command_line 
    Call execute_command_line(command_line)
    OPEN(newunit=filenum_data,file='linenum.txt') 
    READ(filenum_data,*) Num_Entry 
    close(filenum_data)
    print*,'Lines in the file:',Num_Entry   

  if(Simulation_system/='Generic') then
  OPEN(NEWUNIT=filenum_data,FILE=datafile,action='read',status='old')  
  read(filenum_data,*)   
  This_District_id=0
  do this_state=0,Num_Entry-2
     if(this_state==0) then 
        if(Simulation_system=='Single') then 
           allocate(population(1))
           read(filenum_data,*)state_names(1),population(1)
           exit
        else
           read(filenum_data,*)
        end if
     elseif (Simulation_system=='Country') then        
        if(Mobility) then 
        read(filenum_data,*)This_state_id,Dist_Num,This_state_name&
             &,This_district_name,This_population,This_Population_Rural,This_population_urban,this_lat,this_lon   
        else 
        read(filenum_data,*)This_state_id,Dist_Num,This_state_name&
             &,This_district_name,This_population,This_Population_Rural,This_population_urban        
        endif
        print*,'reading',This_state_id,' State: ',trim(This_state_name),' District:',trim(This_district_name)

        If(Dist_Num/=0) This_District_id=This_District_id+1
        if(This_district_name=='STATE') then      
           State_Names(This_state_id)=This_state_name     
           Population(This_state_id)=This_population
           if(Mobility) then 
            state_lat(This_state_id)=this_lat
            state_lon(This_state_id)=this_lon
           end if
           First_district(This_state_id)=This_District_id+1
           if(This_state_id>1) Last_district(This_state_id-1)=This_District_id                
        else
           District_names(This_District_id)=This_district_name
           District_Belongs_To(This_District_id)=This_state_name
           State_ID_of_Dist(This_District_id)=This_state_id
           Population_district(This_District_id)=This_population
           if(Mobility) then 
            dist_lat(This_District_id)=this_lat
            dist_lon(This_District_id)=this_lon
           end if
           if(This_Population_Rural>This_population_urban) is_rural(This_District_id)=.True.            
        end if
        if(This_state_id==total_state_num) Last_district(This_state_id)=This_District_id
        ! This will be replaced and retain the final value
     endif
  end do

  if(Simulation_system=='State'.and.Granularity=='Districts') then
     if(Total_District_Num/=This_District_id) stop 'District number mismatch'
  end if
  close(filenum_data)
  elseif (Simulation_system=='Generic') then  
  
  states=Num_Entry-1 ! First line should be header
  
  allocate(selected_states(states),selected_population(states),Selected_is_rural(states),population(states))  
  OPEN(NEWUNIT=filenum_data,FILE=datafile,action='read',status='old')  
    read(filenum_data,*)
      do this_state=1,States
        if(Mobility) then 
        if(.not.(allocated(state_lat))) allocate(state_lat(States),state_lon(States)) 
        read(filenum_data,*)selected_states(this_state),population(this_state),This_Population_Rural&
        &,This_population_urban,state_lat((this_state)),state_lon((this_state))   
        else 
        read(filenum_data,*)selected_states(this_state),population(this_state),This_Population_Rural&
        &,This_population_urban      
        endif
        if(This_Population_Rural>This_population_urban) Selected_is_rural(this_state)=.True. 
                
      end do  
  close(filenum_data)
      selected_population=population  
  else
  
  Stop 'Not Valid Simulation system'
  end if
  
  print*,'Reading Done' 

  !     do i=1,total_state_num 
  !     print*,state_names(i),First_district(i),Last_district(i)
  !     pause
  !     end do


  !========================================================================
  !========================================================================

  !========================================================================

  ! Now select the number of states we plan to work with
  if(Simulation_system=='Single') then 
     states=1
     allocate(selected_states(states),selected_population(states))
     selected_states(1)=state_names(1)
     selected_population(1)=population(1)
  elseif(Simulation_system=='Country') then
     Num_states=Ini_Read_Int('Number_of_States')
     if(Num_states==total_state_num) then 
        ! Just do all of them. No need to select
        If(Granularity=='States') then
           states=Num_states    
           allocate(selected_states(states),selected_population(states))
           selected_states=state_names
           selected_population=population                
        else 
           states=Total_District_Num
           allocate(selected_states(states),selected_population(states),Selected_is_rural(states)&
                &,selected_district_belongs_to(states),Selected_State_ID_of_DIST(states))                
           forall (This_District=1:states) selected_states(This_District)=trim('State: ')&
                &//trim(adjustl(District_Belongs_To(This_District)))&
                &//trim(' : District: ')//trim(adjustl(district_names(This_District)))
           selected_population=Population_district
           Selected_is_rural=is_rural
           selected_district_belongs_to=District_Belongs_To     
           Selected_State_ID_of_DIST=State_ID_of_DIST
        end if
     else ! Identify the states       
        allocate(Store_selected_states(Num_states))  
        allocate(selected_states_indices(Num_states))

        !=========================================================================
        ! Provide state names
        ! in the file
        Print*,'Reading selected states only'
        datafile=Ini_Read_string('Select_States_Filename')

        OPEN(NEWUNIT=filenum_data,FILE=datafile,action='read',status='old')  
        do this_state=1,Num_states 
           read(filenum_data,*) Store_selected_states(this_state)    
        end do
        close(filenum_data)

        do i=1,Num_states
           if(any(state_names==Store_selected_states(i))) then 
              this_state_num(1)=findloc(state_names,Store_selected_states(i),dim=1) 
              selected_states_indices(i)=this_state_num(1)
              print*,'Selected states are: ',trim(adjustl(Store_selected_states(i)))&
                   &,' with population',int(population(selected_states_indices(i)))
           else
              print*,Store_selected_states(i)
              stop 'Wrong State Name'
           end if
        end do

        if(Granularity=='States') then 
           states=Num_states
           allocate(selected_states(states),selected_population(states))
           selected_states=Store_selected_states
           forall(i=1:states) selected_population(i)=population(selected_states_indices(i))                            
        else
           allocate(Dummy_selected_district_indices(Total_District_Num))
           Print*,'Getting District database'
           states=0
           do i=1,Num_States
              District_counter=states         
              states=states+Last_district(selected_states_indices(i))-First_district(selected_states_indices(i))+1
              !            print*,First_district(selected_states_indices(i)),Last_district(selected_states_indices(i))
              counter=0
              do j=District_counter+1,states
                 counter=counter+1
                 Dummy_selected_district_indices(j)=First_district(selected_states_indices(i))+counter-1
                 !              print*,j,Dummy_selected_district_indices(j)
              end do
              !            pause
           end do

           allocate(selected_states(states),selected_population(states)&
                &,Selected_is_rural(states),selected_district_belongs_to(states),selected_district_indices(states)&
                &,Selected_State_ID_of_DIST(states))                
           selected_district_indices=Dummy_selected_district_indices(1:states) 
           deallocate(Dummy_selected_district_indices)
           !         print*,States
           do i=1,States
              ! Fill in with districts
              selected_states(i)=trim('State: ')//trim(adjustl(District_Belongs_To(selected_district_indices(i))))&
                   &//trim(' : District: ')//trim(district_names(selected_district_indices(i)))           
              !            print*,selected_states(i)

              selected_population(i)=Population_district(selected_district_indices(i))
              Selected_is_rural(i)=is_rural(selected_district_indices(i))
              if(Selected_is_rural(i)) then 
                 selected_states(i)=trim(selected_states(i))//trim(' (Rural)')
              else 
                 selected_states(i)=trim(selected_states(i))//trim(' (Urban)')               
              end if

              selected_district_belongs_to(i)=District_Belongs_To(selected_district_indices(i))
              !            print*,i,selected_district_belongs_to(i)
              Selected_State_ID_of_DIST(i)=State_ID_of_DIST(selected_district_indices(i))
           end do
        end if
     end if
  end if

  print*,'Arranged the population and region names' 

  main_categories=Ini_Read_Int('Number_of_Categories') ! Division can be age-group, socio-economic etc.
  !========================================================================
  allocate(population_fraction(main_categories,states))
  allocate(category_name(main_categories))

  !========================================================================

  if(main_categories==1) category_name(1)='Unified' ! Single category considered
  if(Simulation_system=='Single') then 
     Uni_Pop_Frac=.true.
  else 
     ! population_fraction must be initiated
     Uni_Pop_Frac=Ini_Read_Logical('Uniform_Population_Fraction',.False.)    
  end if

  if(main_categories>1) then
     do i=1,main_categories
        write(this_category,'(i10)') i
        if(Uni_Pop_Frac) then
           if(i<main_categories) then
              key_frac=trim('Population_fraction_')//trim(adjustl(this_category))        
              population_fraction(i,:)=Ini_Read_double(key_frac)
           end if
        end if
        key_name=trim('Name_Category_')//trim(adjustl(this_category))
        category_name(i)=Ini_Read_string(key_name)
     end do
  end if


  !===========================================================================
  ! Identify the state and get the population
  if((.not.(Uni_Pop_Frac)).and.(main_categories>1)) then     
     if((Simulation_system=='Country'.and.Granularity=='States').or.Simulation_system=='Generic') then 
        ALLOCATE(allstate_population_fraction(main_categories,total_state_num))
        print*,'Reading population fraction'
        datafile=Ini_Read_string('Population_Fraction_Filename')    
        OPEN(NEWUNIT=filenum_data,FILE=datafile,action='read',status='old')      
        read(filenum_data,*)     
        if(Simulation_system=='Generic') then 
            all_states=states
        else
            all_states=total_state_num
        end if
        do this_state=1,all_states 
            read(filenum_data,*)state1,allstate_population_fraction(:,this_state)
            if(Simulation_system=='Country'.and.state1/=state_names(this_state)) then
               print*,trim(state1),' Not matching with main list',state_names(this_state)
                stop
            end if
        end do
        close(filenum_data)
       
        if((states==total_state_num).or.Simulation_system=='Generic') then      
           population_fraction=allstate_population_fraction       
        else       
           forall(this_state=1:states) population_fraction(:,this_state)&
                &=allstate_population_fraction(:,selected_states_indices(this_state))
        end if
     elseif((Simulation_system=='Country'.and.Granularity=='Districts')) then 
        ALLOCATE(allstate_population_fraction(main_categories,Total_District_Num))
        print*,'Reading population fraction'
        datafile=Ini_Read_string('Population_Fraction_Filename')    
        OPEN(NEWUNIT=filenum_data,FILE=datafile,action='read',status='old')      
        read(filenum_data,*)     
        do this_state=1,Total_District_Num 
            read(filenum_data,*)state1,allstate_population_fraction(:,this_state)
            if(state1/=district_names(this_state)) then
               print*,trim(state1),' Not matching with main list',district_names(this_state)
                stop
            end if
        end do
        close(filenum_data)
     
        forall(This_District=1:states) population_fraction(:,This_District)&
             &=allstate_population_fraction(:,selected_district_indices(This_District))                
     end if
  end if
  ! Populating the last elements in all states
  forall(i=1:states) population_fraction(main_categories,i)=1d0-sum(population_fraction(1:main_categories-1,i))
  if(any(population_fraction<0)) stop 'Population fractions do not add up to 1'
  !========================================================================
  !========================================================================
!        print*, population_fraction
!        pause
  print*,'Population fraction set up complete' 

  !========================================================================
  
  if(Simulation_system=='Single') Mobility=.False.
  If(Mobility) Mobility_Algorithm=Ini_Read_string('Migration_Algortihm') 
  Check_Mobility:if(Mobility) then 
     What_Mobility_Algorithm:If(Mobility_Algorithm=='Migration-Census2011') then  
        if(Granularity=='States') then  
           allocate(Entire_Migration_matrix(total_state_num,total_state_num))
           Entire_Migration_matrix=0d0
        else 
           stop 'Migration-Census2011 controlled Mobility is only applicable for states' 
        end if
        datafile=Ini_Read_string('Migration_filename')
        Num_corr=Ini_Read_Int('Num_Migration_data') 
        OPEN(NEWUNIT=filenum_data,FILE=datafile,action='read',status='old')  

        do i=1,Num_corr 
           read(filenum_data,*)state1,state2,migration_in_lakhs    
           if(.not.(any(state_names==state1))) then 
              print*,trim(state1),' Not listed in State Database'
              Stop
           else if(.not.(any(state_names/=state2))) then 
              print*,trim(state2),' Not listed in State Database'
              Stop
           end if

           idx(1)=findloc(state_names,state1,dim=1) 
           pop1=population(idx(1)) 
           jdx(1)=findloc(state_names,state2,dim=1) 
           pop2=population(jdx(1)) 
           Entire_Migration_matrix(idx(1),jdx(1))=migration_in_lakhs*100000d0/pop2
           print*,'Indices of the states',idx(1),jdx(1)
        end do
        close(filenum_data)

        Migration_duration=Ini_Read_Int('MOBILITY_DAYS')
        Entire_Migration_matrix=Entire_Migration_matrix/dble(Migration_duration)
        Print*,'Migration Matrix Reading Done: Mobility Matrix will populate from here'
     elseIf(Mobility_Algorithm=='Gravity') then
        const_G=Ini_Read_Double('What_Is_G')
        if(Simulation_system=='Country') then
        if(Granularity=='States') then 
           allocate(Entire_Migration_matrix(total_state_num,total_state_num))
           Entire_Migration_matrix=0d0                        
        else   
           allocate(Entire_Migration_matrix(Total_District_Num,Total_District_Num))
        end if
        elseif(Simulation_system=='Generic') then
           allocate(Entire_Migration_matrix(states,states))
        end if
        Entire_Migration_matrix=0d0
!         print*,size(Entire_Migration_matrix,1),size(Entire_Migration_matrix,2)
!          pause
        do i=1,size(Entire_Migration_matrix,1)
           do j=1,size(Entire_Migration_matrix,2)
              if(i/=j) then
              if((Simulation_system=='Country'.and.Granularity=='States').or.Simulation_system=='Generic')&
                &Entire_Migration_matrix(i,j)=const_G*dble(population(i))&
                &/(haversine(state_lat(i),state_lon(i),state_lat(j),state_lon(j))&
                &*haversine(state_lat(i),state_lon(i),state_lat(j),state_lon(j)))

                if(Simulation_system=='Country'.and.Granularity=='Districts') &
                &Entire_Migration_matrix(i,j)=const_G*dble(Population_district(i))&
                &/(haversine(Dist_lat(i),Dist_lon(i),Dist_lat(j),Dist_lon(j))&
                &*haversine(Dist_lat(i),Dist_lon(i),Dist_lat(j),Dist_lon(j)))                    
 
              end if 
           end do
        end do
        if(Simulation_system=='Generic') then 
        OPEN(NEWUNIT=filenum_result,FILE='stored_data/Migration_matrix/Migration.txt',action='write',status='REPLACE')           
        do i=1,size(Entire_Migration_matrix,1)
            write(filenum_result,*) real(Entire_Migration_matrix(i,:)*population(:))             
        end do
        close(filenum_result)
         end if
     end if What_Mobility_Algorithm
  end if Check_Mobility
  ! Before initializing if the staggered lockdown has to be implemented
  ! We need to break the categories
  Branch_Categories=.False.
  Implement_lockdown=Ini_Read_Logical('Implement_Lockdown',.False.)
  
  If(Implement_Lockdown) then 
    LightSwitch=Ini_Read_Logical('Lightswitch_Lockdown',.False.)
    cyclic_lockdown=Ini_Read_Logical('Cyclic_Lockdown',.False.)
    if(LightSwitch.and.cyclic_lockdown) stop 'LightSwitch and perioridic strategies can not be implemented together'
    if(cyclic_lockdown) then 
        allocate(Staggered_Cyclic(main_categories))    
        Selected_Periodic=Ini_Read_Logical('Selected_Periodic_Lockdown')
        if(Selected_Periodic) then 
            Branch_Categories=.True.
            Cycle_duration_Async=Ini_Read_Int('Cycle_duration_Async')
            Cycle_break_Async=Ini_Read_Int('Cycle_break_Async')

            do i=1,main_categories
                write(category_num,'(i10)') i 
                Staggered_key=trim('Staggered_Category_')//trim(adjustl(category_num))
                Staggered_Cyclic(i)=Ini_Read_Logical(Staggered_key) 
            end do
         else 
           Staggered_Cyclic=.False.             
        end if    
    end if
   end if 

  people_categories=main_categories
  allocate(branch_belongs_to(main_categories))  
  forall(i=1:main_categories) branch_belongs_to(i)=i  
  
  if(Branch_Categories) then           
    allocate(categories_to_divide(main_categories))
    num_branch=Ini_Read_Int('Num_Branches')
    categories_to_divide=Staggered_Cyclic
    call branching(categories_to_divide,num_branch)
  end if
  
!   print*,branch_belongs_to
!   print*,category_name
!   print*,people_categories
!     pause
  call initialize(states,people_categories)

  !========================================================================



  !     print*,selected_states_indices     
  !      print*,population_fraction
  !     pause

  ! Read the final day for simulation
  DAY_FINAL=Ini_Read_double('End_date')    
  steps_days=int(day_final) ! Stepsize of 1 day in saving the data

  
  
  Is_Uniform_contact=Ini_Read_Logical('Use_Uniform_Contacts')
  if(Is_Uniform_contact) then 
    Num_Contact_Places=0
    call get_contact_matrix(Is_Uniform_contact)
    
  else
    if(Implement_Lockdown) then 
        if(Selected_Periodic) stop 'Can not implement selected perioridic with non-Uniform contacts'
    end if
    Num_Contact_Places=Ini_Read_Int('Num_Contact_Places')
    allocate(Base_Matrix_Filename(Num_Contact_Places))
    
    do i=1,Num_Contact_Places    
        write(this_category,'(i10)') i
        key_name=trim('Base_Contact_Matrix_Filename_')//trim(adjustl(this_category))
        Base_Matrix_Filename(i)=Ini_Read_string(key_name)
    end do    
    
    Num_Base_Age_Group=Ini_Read_Int('Base_Age_Group')
    ALLOCATE(Age_bracket(people_categories))
    do i=1,people_categories
        write(this_category,'(i10)') i
        key_bracket=trim('Age_bracket_')//trim(adjustl(this_category))        
        Age_bracket(i)=Ini_Read_double(key_bracket)    
     end do
     
     allocate(base_population_fraction(states,Num_Base_Age_Group))
     population_fraction_filename=Ini_Read_string('Base_Population_Fraction_Filename')
     OPEN(NEWUNIT=contact_filenum,FILE=population_fraction_filename,action='read',status='old')  
            read(contact_filenum,*)
        do i=1,states            
            read(contact_filenum,*)This_name,base_population_fraction(i,:)
        end do    
     close(contact_filenum)
     Write_Derived_Contact=Ini_Read_Logical('Write_Derived_Contact')
    if(Write_Derived_Contact) then 

        allocate(write_contact_name(Num_Contact_Places))
        do i=1,Num_Contact_Places    
            write(this_category,'(i10)') i
            key_name=trim('Contact_Write_Filename_')//trim(adjustl(this_category))
            write_contact_name(i)=Ini_Read_string(key_name)
        end do    

        call get_contact_matrix(Is_Uniform_contact,Base_Matrix_Filename,base_population_fraction,Age_bracket,write_contact_name)
!      print*,branch_belongs_to
!    pause  
        
    else
        call get_contact_matrix(Is_Uniform_contact,Base_Matrix_Filename,base_population_fraction,Age_bracket)
    end if
    if(Implement_Lockdown) then
       allocate(LockDown_Cont_Mat_Coeff(Num_Contact_Places))
        do i=1,Num_Contact_Places    
            write(this_category,'(i10)') i
            key_name=trim('Coefficient_Contact_Lockdown_')//trim(adjustl(this_category))
            LockDown_Cont_Mat_Coeff(i)=Ini_Read_double(key_name)
        end do    
        Lockdown_contact=0d0
        do i=1,Num_Contact_Places
            Lockdown_contact(:,:,:)=Lockdown_contact(:,:,:)+LockDown_Cont_Mat_Coeff(i)*Variational_contact(:,:,:,i)
        end do    
        
        if(Write_Derived_Contact) then 
        do i=1,states
            write_contact_mat='stored_data/contact-store/'//"/"//trim("ELIXSIR_Contact_Lockdown_")//&
             &trim(adjustl(selected_states(i)))//".txt"
            OPEN(NEWUNIT=filenum_result,FILE=write_contact_mat,action='write',status='REPLACE')   
            do j=1,people_categories     
                write(filenum_result,*)Lockdown_contact(j,:,i)
            end do
            close(filenum_result)
        end do    
        end if

    end if
     
    allocate(Unlock_Cont_Mat_Coeff(Num_Contact_Places))
    do i=1,Num_Contact_Places    
       write(this_category,'(i10)') i
       key_name=trim('Coefficient_Contact_Unlock_')//trim(adjustl(this_category))
       Unlock_Cont_Mat_Coeff(i)=Ini_Read_double(key_name)              
    end do  
    Unlock_contact=0d0
    do i=1,Num_Contact_Places
       Unlock_contact(:,:,:)=Unlock_contact(:,:,:)+Unlock_Cont_Mat_Coeff(i)*Variational_contact(:,:,:,i)
    end do    
    if(Write_Derived_Contact) then 
        do i=1,states
            write_contact_mat='stored_data/contact-store/'//"/"//trim("ELIXSIR_Contact_Unlock_")//&
             &trim(adjustl(selected_states(i)))//".txt"
            OPEN(NEWUNIT=filenum_result,FILE=write_contact_mat,action='write',status='REPLACE')   
            do j=1,people_categories     
                write(filenum_result,*)Unlock_contact(j,:,i)
            end do
            close(filenum_result)
        end do    
    end if

    
    
  end if
  ! Read contact Matrix

  Use_Uniform_Init=Ini_Read_Logical('Impose_Uniform_Init',.True.)        
  if(Use_Uniform_Init) then 
     Initial_E(:)=Ini_Read_double('INITIAL_EXPOSED')
     Initial_I_ASY(:)=Ini_Read_double('INITIAL_INFECTED_ASYMPTOMATIC')
     Initial_I_SY(:)=Ini_Read_double('INITIAL_INFECTED_SYMPTOMATIC')
     Initial_I_MILD(:)=Ini_Read_double('INITIAL_INFECTED_MILD')
     Initial_I_SVR(:)=Ini_Read_double('INITIAL_INFECTED_SEVERE')        
     Initial_I_HPTL(:)=Ini_Read_double('INITIAL_INFECTED_HOSPITALIZED')                
  else    
      datafile=Ini_Read_string('Initial_Condition_Filename')    

     if((Simulation_system=='Country'.and.Granularity=='States').or.Simulation_system=='Generic') then
        
        if(Simulation_system=='Generic') then 
            all_states=states
        else
            all_states=total_state_num
        end if
        
        allocate(Initial_E_STORE(all_states),Initial_I_ASY_STORE(all_states)&
                &,Initial_I_SY_STORE(all_states),Initial_I_MILD_STORE(all_states)&
                &,Initial_I_SVR_STORE(all_states),Initial_I_HPTL_STORE(all_states))     
     
        print*,'Reading Initial Conditions'
        OPEN(NEWUNIT=filenum_data,FILE=datafile,action='read',status='old')              
        read(filenum_data,*)     
        do this_state=1,all_states 
            read(filenum_data,*)state1,Initial_E_STORE(this_state),Initial_I_ASY_STORE(this_state)&
                &,Initial_I_SY_STORE(this_state),Initial_I_MILD_STORE(this_state)&
                &,Initial_I_SVR_STORE(this_state),Initial_I_HPTL_STORE(this_state)
            if(Simulation_system=='Country'.and.state1/=state_names(this_state)) then
               print*,trim(state1),' Not matching with main list'
                stop
            end if
        end do
        close(filenum_data)
        
        if(Simulation_system=='Generic') then
        
            Initial_E=Initial_E_STORE
            Initial_I_ASY=Initial_I_ASY_STORE
            Initial_I_SY=Initial_I_SY_STORE
            Initial_I_MILD=Initial_I_MILD_STORE
            Initial_I_SVR=Initial_I_SVR_STORE        
            Initial_I_HPTL=Initial_I_HPTL_STORE                
        
        else
        forall (this_state=1:states)
            Initial_E(this_state)=Initial_E_STORE(selected_states_indices(this_state))
            Initial_I_ASY(this_state)=Initial_I_ASY_STORE(selected_states_indices(this_state))
            Initial_I_SY(this_state)=Initial_I_SY_STORE(selected_states_indices(this_state))
            Initial_I_MILD(this_state)=Initial_I_MILD_STORE(selected_states_indices(this_state))
            Initial_I_SVR(this_state)=Initial_I_SVR_STORE(selected_states_indices(this_state))        
            Initial_I_HPTL(this_state)=Initial_I_HPTL_STORE(selected_states_indices(this_state))                
        end forall    
        end if
        elseif(Simulation_system=='Country'.and.Granularity=='Districts') then         
        allocate(Initial_E_STORE(Total_District_Num),Initial_I_ASY_STORE(Total_District_Num)&
                &,Initial_I_SY_STORE(Total_District_Num),Initial_I_MILD_STORE(Total_District_Num)&
                &,Initial_I_SVR_STORE(Total_District_Num),Initial_I_HPTL_STORE(Total_District_Num))          
        print*,'Reading Initial Conditions'

        OPEN(NEWUNIT=filenum_data,FILE=datafile,action='read',status='old')              
        read(filenum_data,*)     
        do this_state=1,Total_District_Num 
            read(filenum_data,*)state1,Initial_E_STORE(this_state),Initial_I_ASY_STORE(this_state)&
                &,Initial_I_SY_STORE(this_state),Initial_I_MILD_STORE(this_state)&
                &,Initial_I_SVR_STORE(this_state),Initial_I_HPTL_STORE(this_state)
            if(state1/=district_names(this_state)) then
               print*,trim(state1),' Not matching with main list'
                stop
            end if
        end do
        close(filenum_data)
        
        forall (this_state=1:states)
            Initial_E(this_state)=Initial_E_STORE(selected_district_indices(this_state))
            Initial_I_ASY(this_state)=Initial_I_ASY_STORE(selected_district_indices(this_state))
            Initial_I_SY(this_state)=Initial_I_SY_STORE(selected_district_indices(this_state))
            Initial_I_MILD(this_state)=Initial_I_MILD_STORE(selected_district_indices(this_state))
            Initial_I_SVR(this_state)=Initial_I_SVR_STORE(selected_district_indices(this_state))        
            Initial_I_HPTL(this_state)=Initial_I_HPTL_STORE(selected_district_indices(this_state))                
        end forall    

        
     end if           
  end if
  !      print*,Initial_I_SY,Initial_I_ASY

  BETA_STATE_VARIATION=Ini_Read_Logical('BETA_STATE_VARIATION')

  if(BETA_STATE_VARIATION) then 
    if(Simulation_system=='Generic') then 
        all_states=states
    else
        all_states=total_state_num
    end if
    BETA_FILENAME=Ini_Read_string('BETA_FILE')
    allocate(beta_array(all_states),beta_lockdown_array(all_states))
    OPEN(NEWUNIT=filenum_data,FILE=BETA_FILENAME,action='read',status='old')          
    do i=1,all_states
        if(Implement_Lockdown) then 
            READ(filenum_data,*)this_state_name,beta_array(i),this_lockdown,beta_lockdown_array(i)
        else 
            READ(filenum_data,*)this_state_name,beta_array(i)
        end if    
    end do
    close(filenum_data)
    if(Simulation_system=='Generic')then 
       selected_beta_array=beta_array
    else 
    if(Granularity=='States') forall(i=1:states) selected_beta_array(i)=beta_array(selected_states_indices(i))
    if(Granularity=='Districts') forall(i=1:states) selected_beta_array(i)=beta_array(Selected_State_ID_of_DIST(i))
    end if
  else   
    selected_beta_array(1:states)=Ini_Read_double('BETA_PARAM')
  end if
  E_ASY=Ini_Read_double('Exposed_to_Asymptomatic')
  E_SY=Ini_Read_double('Exposed_to_Symptomatic')
  ASY_R=Ini_Read_double('Asymptomatic_to_Recovered')
  SY_M=Ini_Read_double('Symptomatic_to_Mild')
  M_R=Ini_Read_double('Mild_to_Recovered')    
  SY_SV=Ini_Read_double('Symptomatic_to_Severe')
  SV_H=Ini_Read_double('Severe_to_Hospitalized')    
  SV_R=Ini_Read_double('Severe_to_Recovered')    
  SV_D=Ini_Read_double('Severe_to_Dead')        
  H_D=Ini_Read_double('Hospitalized_to_Dead')    
  H_R=Ini_Read_double('Hospitalized_to_Recovered')    
 


  Initial_eff_asymp=Ini_Read_double('Efficiency_asymptomatic')    
  Initial_eff_symp=Ini_Read_double('Efficiency_symptomatic')    
  Initial_eff_mild=Ini_Read_double('Efficiency_mild')    
  Initial_eff_svr=Ini_Read_double('Efficiency_severe')    


  Implement_Rapid_Quarantine_Effect=Ini_Read_Logical('Implement_Rapid_Test_N_Quarantine_Effect',.False.) 
  if(Implement_Rapid_Quarantine_Effect) then 
     decay_time_asymp=Ini_Read_double('Tau_ASYMP')    
     decay_time_symp=Ini_Read_double('Tau_SYMP')    
     decay_time_mild=Ini_Read_double('Tau_Mild')    
     decay_time_svr=Ini_Read_double('Tau_SVR')    

     decay_time_asymp_unlock=Ini_Read_double('Tau_ASYMP_Unlock')    
     decay_time_symp_unlock=Ini_Read_double('Tau_SYMP_Unlock')    
     decay_time_mild_unlock=Ini_Read_double('Tau_Mild_Unlock')    
     decay_time_svr_unlock=Ini_Read_double('Tau_SVR_Unlock')    
  else 
     decay_time_asymp=1d30
     decay_time_symp=1d30
     decay_time_mild=1d30
     decay_time_svr=1d30

     decay_time_asymp_Unlock=1d30
     decay_time_symp_Unlock=1d30
     decay_time_mild_Unlock=1d30
     decay_time_svr_Unlock=1d30
     
  end if

  if(people_categories==1) then 
     Uniform_fraction=.true.
  else    
     Uniform_fraction=Ini_Read_Logical('Uniform_Fraction',.False.)
  end if
  ! We assume Asymptomatic fraction is age independent
  fraction_as=Ini_Read_double('Asymptomatic_fraction')    
  fraction_HPTL=Ini_Read_double('Hospitalized_fraction')

  if(Granularity=='Districts') then 
     do k=1,states
        if(Selected_is_rural(k)) then 
           fraction_HPTL(:,k)=Ini_Read_double('Hospitalized_fraction_Rural')
        else
           fraction_HPTL(:,k)=Ini_Read_double('Hospitalized_fraction_Urban')            
        end if
     end do
  end if

  if(Uniform_fraction) then 
     fraction_as(:)=Ini_Read_double('Asymptomatic_fraction')      
     fraction_mild(:)=Ini_Read_double('Mild_fraction')    
     fraction_dead(:)=Ini_Read_double('Death_fraction')    
  else
     do i=1,people_categories
     
        write(this_category,'(i10)') branch_belongs_to(i)
        key_asymp=trim('Asymptomatic_fraction_Age_Group_')//trim(adjustl(this_category))                
        key_mild=trim('Mild_fraction_Age_Group_')//trim(adjustl(this_category))        
        key_dead=trim('Death_fraction_Age_Group_')//trim(adjustl(this_category))

        fraction_as(i)=Ini_Read_double(key_asymp)
        fraction_mild(i)=Ini_Read_double(key_mild)    
        fraction_dead(i)=Ini_Read_double(key_dead)    
     end do

  end if

!     print*,fraction_mild
!     print*,fraction_dead
!     pause
  ratio_dead_NH_H=Ini_Read_double('Death_Ratio_NH_H')
  fraction_NH_dead(:)=ratio_dead_NH_H*fraction_dead(:)

  if(any(fraction_NH_dead>1)) stop 'Death fraction more than hospitalized: reduce Death_Ratio_NH_H'




  ! Intervention
  !     pause 'Implement zero infectivity when all channels apart from H,R and D are less than 1'

  if(Implement_lockdown) then 
     allocate(lockdown(states,people_categories,int(DAY_FINAL)))
     lockdown=.False.

     if(LightSwitch) then 

        Lightswitch_Threshold=Ini_Read_Double('Lightswitch_Threshold')
        Lightswitch_duration=Ini_Read_Int('Lightswitch_Lockdown_Duration')
        Hospital_Controlled=Ini_Read_Logical('Lightswitch_Hospital_Control')
        if(Hospital_Controlled) then 
           Hospital_Threshold=Lightswitch_Threshold
           Lightswitch_off_Tol=Ini_Read_Double('Lightswitch_Off_Tolerence')
        end if


    elseif(cyclic_lockdown) then 
   
        
        start_with_Lockdown=Ini_Read_Logical('Start_With_Lockdown')
        Cycle_start_day=Ini_Read_Int('Lockdown_Cycle_Start')    
        Cycle_duration=Ini_Read_Int('Lockdown_Cycle_Duration')    
        Cycle_break=Ini_Read_Int('Lockdown_Cycle_Break')
        allocate(first_phase(people_categories))
        if(start_with_Lockdown) then
        lockdown=.True.
        do i=1,main_categories 
            
            if(Staggered_Cyclic(i)) then 
                do j=1,num_branch
                    first_phase(branch_begin(i)+j-1)=Cycle_start_day-(j-1)*Cycle_break_Async
                    LockDown(:,branch_begin(i)+j-1,max(first_phase(branch_begin(i)+j-1),1):int(DAY_FINAL))=.False.
                end do
            else 
                ! Cycle starts with workdays after initial lockdown phase
                ! only one branch in sync categories
                first_phase(branch_begin(i))=Cycle_start_day-Cycle_duration            
                LockDown(:,branch_begin(i),max(first_phase(branch_begin(i)),1):int(DAY_FINAL))=.False.                
            end if
            
            print*,first_phase
            pause
        end do    
        else 
            lockdown=.False.
            first_phase(:)=Cycle_start_day
            !forall (i=1:people_categories) LockDown(:,i,1:first_phase(i))=.False.             
        end if    
        do i=1,people_categories        
            this_day=1       
            This_Cycle_start_day=first_phase(i)
            
            if(Staggered_Cyclic(branch_belongs_to(i))) then
                if(num_branch>1&
                &.and.mod(Cycle_duration,(num_branch-1)*Cycle_break)/=0)&
                & stop 'Lockdays has to be multiple of (Num_Branch-1)*Work Days'
            
            end if
            
            print*,i,This_Cycle_start_day
            pause
            do while(this_day<int(DAY_FINAL))
                this_day=This_Cycle_start_day
               
                if(Staggered_Cyclic(branch_belongs_to(i))) then
                    lockdown(1:states,i,max(this_day,1):min(this_day+Cycle_duration_Async-1,int(DAY_FINAL)))=.True.            
                    This_Cycle_start_day=this_day+Cycle_duration_Async+Cycle_break_Async
                else
                    lockdown(1:states,i,max(this_day,1):min(this_day+Cycle_duration-1,int(DAY_FINAL)))=.True.            
                    This_Cycle_start_day=this_day+Cycle_duration+Cycle_break                
                end if    
            end do      
        end do
        !         do i=1,steps_days 
        !             print*,i,lockdown(states,i)
        ! !             pause
        !         end do
        !         pause
     else 
        Num_Lockdown=Ini_Read_Int('Number_of_Lockdowns')

        do i=1,Num_Lockdown
           write(this_lockdown,'(i10)') i

           key_lock_day=trim('Day_of_Lockdown_')//trim(adjustl(this_lockdown))
           key_lock_duration=trim('Duration_of_Lockdown_')//trim(adjustl(this_lockdown))
           Lockdown_start_day=Ini_Read_Int(key_lock_day)    
           Lockdown_duration=Ini_Read_Int(key_lock_duration)    
           lockdown(1:states,:,Lockdown_start_day:Lockdown_start_day+Lockdown_duration-1)=.True.            
        end do
     end if
     allocate(selected_beta_lockdown_array(states))     
     if(BETA_STATE_VARIATION) then    
        if (Simulation_system=='Generic') then
            selected_beta_lockdown_array=beta_lockdown_array
        else 
        if(Granularity=='States') forall(i=1:states) selected_beta_lockdown_array(i)&
            &=beta_lockdown_array(selected_states_indices(i))
        if(Granularity=='Districts') forall(i=1:states) selected_beta_lockdown_array(i)&
            &=beta_lockdown_array(Selected_State_ID_of_DIST(i))
        end if    
     else    
        selected_beta_lockdown_array(1:states)=Ini_Read_double('Beta_Lockdown')
     end if
     eff_mob_block=Ini_Read_double('Mobility_Block_Lockdown')    
  end if

!   do i=1,people_categories
!     write(10,*)i,lockdown(1,i,:)
!   end do
!   stop
  Print*,'Getting Initial Conditions'
  call Calc_initial_conditions(initial_conditions,selected_population)         


  allocate(save_day(steps_days))
  allocate(SEIR_STORE_ARRAY(steps_days,num_base_eq,people_categories,states))
  allocate(SEIR_New_Infected_Daily(steps_days,states))
  allocate(SEIR_Cumulative_Infected(steps_days,states))
  ALLOCATE(SEIR_Cumulative_Dead(steps_days,states))

  forall(i=1:steps_days) save_day(i)=day_one+((DBLE(I)-1.0_WP)*(day_final-day_one)/(DBLE(steps_days)-1.0_WP))    

  Print*,'Integrating'
  !     print*,save_day
  call integrate(initial_conditions,SEIR_STORE_ARRAY,save_day)    
  Print*,'Integration done'

  ! Store new and cumulative infected

  forall(k=1:states) 
     SEIR_New_Infected_Daily(1,k)=sum(IASY_INI(:,k)+ISY_INI(:,k)+IM_INI(:,k)&
          &+ISV_INI(:,k)+IH_INI(:,k)+D_ini(:,k)+R_ini(:,k))
     SEIR_Cumulative_Infected(1,k)=SEIR_New_Infected_Daily(1,k)                
     SEIR_Cumulative_Dead(1,k)=sum(D_ini(:,k))
  end forall

  do i=2,steps_days
     do k=1,states
        SEIR_New_Infected_Daily(I,k)=sum(SEIR_STORE_ARRAY(I,Asymptomatic,:,K)&
             &+SEIR_STORE_ARRAY(I,symptomatic,:,K)+SEIR_STORE_ARRAY(I,mild,:,K)&
             &+SEIR_STORE_ARRAY(I,severe,:,K)+SEIR_STORE_ARRAY(I,hospitalized,:,K)&
             &+SEIR_STORE_ARRAY(I,dead,:,K)+SEIR_STORE_ARRAY(I,Recovered,:,K))&
             &-SEIR_Cumulative_Infected(I-1,k)

        SEIR_Cumulative_Infected(I,k)=SEIR_Cumulative_Infected(I-1,k)&
             &+SEIR_New_Infected_Daily(I,k)
        SEIR_Cumulative_Dead(I,k)=sum(SEIR_STORE_ARRAY(I,DEAD,:,K))    
     end do
  end do


  ! Writing the results in plot_data folder 
  foldername='plot_data'
  do k=1,states    
     write_infected_filename(k)=trim(adjustl(foldername))//"/"//trim("ELIXSIR_CUMULATIVE_OUT_")//&
          &trim(adjustl(selected_states(k)))//".txt"    

     OPEN(NEWUNIT=filenum_result_infected,FILE=write_infected_filename(k),action='write',status='REPLACE')   
     write(filenum_result_infected,*) 'Day Daily-New-Infected Cumulative-infected Dead'  
     do I=1,steps_days            
        write(filenum_result_infected,*)int(save_day(I)),SEIR_New_Infected_Daily(I,K)&
             &,SEIR_Cumulative_Infected(I,K),SEIR_Cumulative_Dead(I,K)
     end do
     do j=1,people_categories
        if(people_categories==1) then 
            category_num='Unified'
        else
            write(category_num,'(i10)') j        
        end if    
        write_filename(j,k)=trim(adjustl(foldername))//"/"//trim("ELIXSIR_OUT_")//&
             &trim(adjustl(selected_states(k)))//trim("_people_category_")//trim(adjustl(category_num))//".txt"
        OPEN(NEWUNIT=filenum_result,FILE=write_filename(j,k),action='write',status='REPLACE')   
        write(filenum_result,*) 'Day Susceptible Exposed Asymptomatic Symptomatic Mild &
        &Severe Hospitalized Dead Recovered Combined-Hospitalized Lockdown'
        do I=1,steps_days            
           write(filenum_result,*)int(save_day(I)),SEIR_STORE_ARRAY(I,:,J,K),&
           &sum(SEIR_STORE_ARRAY(I,Hospitalized,:,K)),LockDown(k,j,I)
        end do
        close(filenum_result)
     end do
     close(filenum_result_infected)
  end do

  Print*,'Data files written'

  ! This creat plot script  
  call makeplots(write_filename,write_infected_filename,selected_states,category_name)
end program elixsir
