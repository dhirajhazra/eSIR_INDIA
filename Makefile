ifortErr = $(shell which ifort >/dev/null; echo $$?)
ifeq "$(ifortErr)" "0"
F90C     = ifort
FFLAGS   = -qopenmp -O2 -fpp -standard-semantics -no-wrap-margin
DEBUGFLAGS= -qopenmp -O0 -ip -W0 -WB -fpp -traceback -check all -debug all -standard-semantics -no-wrap-margin
else
#gfortran compiler
F90C     = gfortran
FFLAGS   = -Ofast -fopenmp -ffree-form 
DEBUGFLAGS = -cpp -Og -g -fbounds-check -fbacktrace -ffree-line-length-none
endif


# This is default code
Driver = elixsir
OBJFILES = inifile.o rksuite.o parameters.o syseq.o utils.o engine.o $(Driver).o

export FFLAGS
export DEBUGFLAGS
export F90C


%.o: %.f90
	$(F90C) $(FFLAGS) -c $*.f90
#	$(F90C) $(DEBUGFLAGS) -c $*.f90

all :	elixsir

elixsir:	$(OBJFILES)
	$(F90C) $(FFLAGS) $(OBJFILES) -o elixsir

debug:	$(OBJFILES)
	$(F90C) $(DEBUGFLAGS) $(OBJFILES) -o elixsir

run :
	time ./elixsir parameters-generic.ini

plots :
	gnuplot > load 'plots.p'

clean:
	rm -f *.o *.mod *.d *.pc *.obj fort.* *.out plot_data/* stored_data/contact-store/* stored_data/Migration_matrix/* elixsir 

