# Designed by Dhiraj Kumar Hazra, IMSc, Chennai (dhirajhazra@gmail.com)
# This is a standard parameters file.
# The parameters for the run will be read from here
# Note that for matrices, it is not possible to read from here. 
# Therefore you need to supply separate file from where the code can read
# As of now we use constants in many coupling
# We have these categories
#susceptible=1,exposed=2,asymptomatic=3
#symptomatic=4,mild=5,severe=6
#hospitalized=7,dead=8,recovered=9
#(The numbers are to recognize the Columns in the files in putput plot_data directory)
#First column will be days and the next 9 will be these categories in the same order 

# System to simulate 
# Three options are there
# Single, State
System=Country

# If States, provide the number of states 
# How many Number_of_States you want in your simulation
Number_of_States=5

# Granularity
# Whether to simulate States or Districts
Granularity=States

#Where from the state data will be read. Columns should be
#St_Code Dist_Code State Dist Population_Total Population_Rural Population_Urban
#This data is from GOI Census 2011. If you need to use projected data or different data
#open a new file, edit the numbers and use that file to be read
State_filename=input_data/country/INDIA.dat

#If the number of states are less than 36, i.e. you are not using all states
#then you need to provide a file where you just list the states in each line
#The names should be same as in State_filename 
Select_States_Filename=input_data/country/selected_states.dat

#Number of categories (lets say age group)

Number_of_Categories=7

#If categories are more than one specify fraction of population in each category
# If only one category is there the Population_fraction will not be read
# You must specify all fractions for all categories but the last. 
# Last fraction will be 1-sum(other fractions) 
# If all states having same fraction
# For system=Single uniform_population_fraction is always True and the value must be provided below
Uniform_Population_Fraction=T
Population_fraction_1=0.293919744611773
Population_fraction_2=0.372859785190776
Population_fraction_3=0.139403574004092
Population_fraction_4=0.095272938408626
Population_fraction_5=0.058971455468919
Population_fraction_6=0.028596257024491
Population_fraction_7=0.010976245291323
# If Uniform_Population_Fraction=F
# Provide filename
# Columns will be: StateName population_fraction[i=1,Number_of_Categories]
Population_Fraction_Filename=input_data/STATE-Population-Fraction.dat
# Category Name
Name_Category_1=Age:0-19
Name_Category_2=Age:20-39
Name_Category_3=Age:40-49
Name_Category_4=Age:50-59
Name_Category_5=Age:60-69
Name_Category_6=Age:70-79
Name_Category_7=Age:>80

# Your simulation starts from day 1 and here you provide the end date
End_date=300

#Initial values
#Initial values should differ in each states and categories
#It should be read from a file
#If they are same across states, then 
#These numbers represent total initial number within all categories
Impose_Uniform_Init=T
INITIAL_EXPOSED=700
INITIAL_INFECTED_ASYMPTOMATIC=35
INITIAL_INFECTED_SYMPTOMATIC=70
INITIAL_INFECTED_MILD=0
INITIAL_INFECTED_SEVERE=0        
INITIAL_INFECTED_HOSPITALIZED=0                

#If Impose_Uniform_Init=F and Number_of_States=36
# then an input file must be provided in the same order of State_filename
# where first input will be name of the states and next 3 input will be 
# INITIAL_EXPOSED, INITIAL_INFECTED_ASYMPTOMATIC and INITIAL_INFECTED_SYMPTOMATIC respectively
# FOR DISTRICT LEVEL GRANULARITY, for now only uniform is allowed
Initial_Condition_Filename=input_data/country/State_Initials.dat
#If you want to use selected_states from Select_States_Filename you should write the numbers in the same
#order as in State_filenames


#Coupling (coupling between susceptible and infected)
BETA_PARAM=0.095
# State Dependent Beta
BETA_STATE_VARIATION=F
#If BETA_STATE_VARIATION=T, provide filename
# input_data/STATE_BETA_LOCK.dat contains all states.
# If all states not used, provide file name 
BETA_FILE=input_data/State_Beta_Variation.dat

###############################################################
###########  Contacts #########################################
###############################################################
# Uniform contacts across all age groups
Use_Uniform_Contacts=F

# For Non-Uniform contacts
# If the contact matrix is provided for the age groups to be used in simulation
Tailor_Made_Contact=F
# if Tailor_Made_Contact=T, you must provide the contact matrices for all the 
# regions (states/districts etc.) in the simulations
# here you can provide the extension and put the files in contact_matrix folder in input_data folder
# How many places should you distribute your contacts. Here is an example of 4 places
Num_Contact_Places=4
# namely Home, Work, School and Others
Prepared_Contact_Extension_1=stored_data/contact-store/Reduced-Contact-Home
Prepared_Contact_Extension_2=stored_data/contact-store/Reduced-Contact-Work
Prepared_Contact_Extension_3=stored_data/contact-store/Reduced-Contact-School
Prepared_Contact_Extension_4=stored_data/contact-store/Reduced-Contact-Others

# if Tailor_Made_Contact=F, prepare contact matrices from base matrices (Prem, Cook, Jit)
Base_Contact_Matrix_Filename_1=input_data/Base_Contact_Home.dat
Base_Contact_Matrix_Filename_2=input_data/Base_Contact_Work.dat
Base_Contact_Matrix_Filename_3=input_data/Base_Contact_School.dat
Base_Contact_Matrix_Filename_4=input_data/Base_Contact_Others.dat

# The portion of contacts during lockdown and unlock timescale
# Lockdown

Coefficient_Contact_Lockdown_1=1
Coefficient_Contact_Lockdown_2=0
Coefficient_Contact_Lockdown_3=0
Coefficient_Contact_Lockdown_4=0

# unlock
# for Chennai Unlock 50% people are working at workplaces, no schools 
# and public transports also are not available 

Coefficient_Contact_Unlock_1=1
Coefficient_Contact_Unlock_2=0.5
Coefficient_Contact_Unlock_3=0
Coefficient_Contact_Unlock_4=0

# Size of base contact (how many age groups in Base_Contact_Matrix)
Base_Age_Group=17

# Base population fraction filename
# First row header
# First column Name, rest of the columns contain population fractions for Base_Age_Group
Base_Population_Fraction_Filename=input_data/country/Base-STATE-Population-Fraction.dat

# Age brackets in base to combine in reduced age group
Age_bracket_1=4
Age_bracket_2=4
Age_bracket_3=2
Age_bracket_4=2
Age_bracket_5=2
Age_bracket_6=2
Age_bracket_7=1

#If you want the code to write the derived reduced contact matrices
Write_Derived_Contact=T
# Then provide the filename to write: extension will be automatically created with
# corresponding StateName
Contact_Write_Filename_1=stored_data/contact-store/Reduced-Contact-Home
Contact_Write_Filename_2=stored_data/contact-store/Reduced-Contact-Work
Contact_Write_Filename_3=stored_data/contact-store/Reduced-Contact-School
Contact_Write_Filename_4=stored_data/contact-store/Reduced-Contact-Others
###############################################################
# Whether to include Incoming and outgoing MOBILITY between states. 
# This is whether to allow it within the system of equations.
# This is not related to lockdown. Option for lockdown is there afterwards
# No Mobility option for District GRANULARITY for now
Allow_Mobility=F
#File should be provided
# From the states in the second column people migrates to the states in the 
# first column. Third column is number in lakh (100000) units.
#Taken from the census 2011 data. Note that these are long term
# Migration and the system we are trying to solve needs daily migration
# However this should be propotional to that as the census table shows migration
# is most in neighboring states or states with higher job opportunities.
# This should follow for daily migration too.
# First and second column will have state numbers without spaces and special Characters
# Such as Jammu & Kashmir should be written as JammuKashmir
Migration_filename=input_data/Migration.dat
#Data taken from https://public.flourish.studio/visualisation/526207/embed?auto=1 . 
#Many neighboring states have very little migration, North-East states are examples. 
#Does it mean that daily exchange of people is also less ? 
#How many entries are there in the Migration file
Num_Migration_data=101
# We need to rescale the migration to days. Say the numbers in 3rd column were accumulated in
# 6 months.
MOBILITY_DAYS=180
# District Level Migration: Gravity
#https://www.emerald.com/insight/content/doi/10.1108/17479894200900011/full/html?skipTracking=true
#"In inter-state migration, the incidence of rural-to-rural and urban-to-urban movements is at almost the same level: 26.6% and 26.7% respectively."
#http://censusindia.gov.in/Data_Products/Data_Highlights/Data_Highlights_link/data_highlights_D1D2D3.pdf
#8.8 million migration for duration less than 1 year
Migration_Algortihm=Gravity

What_Is_G=2e-6


#Coupling between different compartments. This should take care of the 
#transition time. Unit 1/days
#Coefficient for Exposed to Asymptomatic
Exposed_to_Asymptomatic=0.5 
#0.192 
#Coefficient for Exposed to symptomatic
#should be the same as asymptomatic as we take care of the share of 
#both branches next
Exposed_to_Symptomatic=0.5 
#0.192
##Coefficient for Asymptomatic to recovered (all of them, so the time is only factor)
Asymptomatic_to_Recovered= 0.1428
#Coefficient for symptomatic to mild
Symptomatic_to_Mild= 0.5
#Coefficient for symptomatic to severe
#should be same as Symptomatic_to_Mild unless we decide to change it
Symptomatic_to_Severe=0.5
#Coefficient for Mild to Recovered
Mild_to_Recovered=0.1428      
#Coefficient for severe to hospitalized
Severe_to_Hospitalized= 0.1736
#Coefficient for severe to recovered
Severe_to_Recovered=0.048
#Coefficient for severe to dead
Severe_to_Dead=0.048
#Coefficient for hospitalized to dead
Hospitalized_to_Dead= 0.068
#Coefficient for hospitalized to Recovered
#Can be different from Hospitalized_to_Dead
#But does not affect other compartments
# Will be crucial to compare with death rate data
Hospitalized_to_Recovered= 0.068


#efficiency of different infected people to infect
#This is simply what we multiply the beta term in the equations.
Efficiency_asymptomatic=0.67
Efficiency_symptomatic=1
Efficiency_mild=1
Efficiency_severe=1


#How mauch fractions of total infected are asymptomatic
#Report from Iceland says 0.8
#This will multiply the Exposed_to_Asymptomatic rate which is in (1/days)
#Exposed_to_Symptomatic on the other hand will be multiplied by 1-Asymptomatic_fraction
Asymptomatic_fraction=0.33

#Symptomatic bifurcates to mild and severe. Here you put the mild fraction. Severe will be the rest
Uniform_Fraction=F
# If Uniform_Fraction=F provide age dependent fractions
Asymptomatic_fraction_Age_Group_1=0.50
Asymptomatic_fraction_Age_Group_2=0.45
Asymptomatic_fraction_Age_Group_3=0.4
Asymptomatic_fraction_Age_Group_4=0.35
Asymptomatic_fraction_Age_Group_5=0.3
Asymptomatic_fraction_Age_Group_6=0.25
Asymptomatic_fraction_Age_Group_7=0.2
Asymptomatic_fraction_Age_Group_8=0.15
Asymptomatic_fraction_Age_Group_9=0.1


Mild_fraction=0.956
# If Uniform_Fraction=F provide age dependent fractions
Mild_fraction_Age_Group_1=0.988
Mild_fraction_Age_Group_2=0.988
Mild_fraction_Age_Group_3=0.951
Mild_fraction_Age_Group_4=0.892
Mild_fraction_Age_Group_5=0.834
Mild_fraction_Age_Group_6=0.757
Mild_fraction_Age_Group_7=0.727

#Fraction of people that will die 
Death_fraction=0.2
# If Uniform_Fraction=F provide age dependent fractions

Death_fraction_Age_Group_1=0.025
Death_fraction_Age_Group_2=0.025
Death_fraction_Age_Group_3=0.03
Death_fraction_Age_Group_4=0.055
Death_fraction_Age_Group_5=0.132
Death_fraction_Age_Group_6=0.2
Death_fraction_Age_Group_7=0.34


# The fraction severe of people who get health care in hospitalized
Hospitalized_fraction=1
#0.7
# If Granularity=Districts
# Then provide fraction of hospitalized people in Urban and Rural areas
# Not needed otherwise
Hospitalized_fraction_Rural=0.3
Hospitalized_fraction_Urban=0.8
# Also the death ratios between Hospitalized and non-hospitalized 
# ratio=2 means twice more people can die in severe compartment 
# if not hospitalized
Death_Ratio_NH_H=2

# INTERVENTION
#  You need to mention whether to implement lockdown
Implement_Lockdown = T

# Three types of lockdown can be implemented. 
# Nationwide, lighswitch and Cyclic
# Only one can be true. For Nationwide lockdown no logical statement is needed. 
# Just keep other two false.

# Nationwide lockdown
Number_of_Lockdowns= 2

# If True, only then the lockdown details will be read 
# Day on which lockdown is announced. from start date
Day_of_Lockdown_1=6
# for how many days
Duration_of_Lockdown_1=68  

#Note that the second lockdown date has to be more than the first lockdown + Duration of first Lockdown
# The code will not throw any errors if there is an overlap. It will simply lockdown the overlapping days
Day_of_Lockdown_2=92 
# for how many days
Duration_of_Lockdown_2=17    

Day_of_Lockdown_3=150 
# for how many days
Duration_of_Lockdown_3=40    

Day_of_Lockdown_4=140 
# for how many days
Duration_of_Lockdown_4=10    

# Whether to implement lightswitch
# If lightswitch activated discrete Lockdowns will not be used
Lightswitch_Lockdown=F
# When to implement. Provide the number of infected people (Symptomatic+Severe+Mild) 
#beyond which lockdown is imposed on that state. This is a state dependent lockdown
Lightswitch_Threshold=500
# For How many days lightswitch lockdown will be there
Lightswitch_Lockdown_Duration=14
# If lightswitch is based on hospitalized cases
# For this, both Lightswitch_Lockdown and following must be set true
Lightswitch_Hospital_Control=F
# It will use the same Lightswitch_Threshold
# You need to provide the switch off tolerence 
Lightswitch_Off_Tolerence=100

# Periodic Lockdown
Cyclic_Lockdown = F
# When the Periodic should start
Lockdown_Cycle_Start=30    
# How many days of lockdown in each Periodic 
Lockdown_Cycle_Duration=28    
# How many days of break from lockdown you would like to provide
Lockdown_Cycle_Break=7
# For Periodic Async/Sync lockdown
# Few age groups follow synchronous and few follow asynchronous
Selected_Periodic_Lockdown=F
# Number of Staggered WorkGroup
Num_Branches=3
# If Selected Periodic Lockdown is implemented

Staggered_Category_1=T
Staggered_Category_2=T
Staggered_Category_3=T
Staggered_Category_4=T
Staggered_Category_5=T
Staggered_Category_6=T
# Lockdown_Cycle_Duration and Lockdown_Cycle_Break will be 
# used for selected staggered cases
# in selected synchronous cases, provide the cycle duration and break
Cycle_duration_Async=14
Cycle_break_Async=7

# If Periodic lockdown follows a simple lockdown
Start_With_Lockdown=F

# with what factor, beta will be blocked, depends on the severity of the lockdown
Beta_Lockdown=0.06
# with what factor, Mobility will be blocked, depends on the state border control
Mobility_Block_Lockdown=0.1    

# Increase in testing
# It is assumed that testing increases exponentially
# Therefore more people will be quarantined and it will not infect susceptibles
# We have to provide a timescale for that.
# We are using an exponential decease in the infectiousness of different compartments
# If we use same timescale for all, it effectively means reducing beta uniformly for all categories
# However in reality
Implement_Rapid_Test_N_Quarantine_Effect= T
Tau_ASYMP=300
Tau_SYMP=150
Tau_Mild=150
Tau_SVR=150

Tau_ASYMP_Unlock = 480
Tau_SYMP_Unlock = 240
Tau_Mild_Unlock = 240
Tau_SVR_Unlock = 240
